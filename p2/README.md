# Практическая работа №2

## Исходные данные

    * Ноутбук MSI
    * образ ubuntu из прошлой практики
    * OpenCTI
    
## Цель работы

1. Установить OpenCTI 

## Выполение работы

### Создать репозиторий в GitLab.
1. Запустим образ
```
docker start myimage
docker attach myimage
```
2. Обновим и получим необходиме пакеты при помощи следующих команд, а также установим докер на образ инструкция(https://losst.ru/ustanovka-docker-na-ubuntu-16-04).
```
apt update
apt-get install sudo
apt-get install docker-compose
```
3. Клонируем репозиторий с OpenCTI
```
mkdir /path/to/your/app && cd /path/to/your/app
git clone https://github.com/OpenCTI-Platform/docker.git
cd docker
```
4. Редактирум файл. Что бы иметь возможность редактировать подгружаем nano в докер.
```
sudo apt-get install nano
sudo nano docker-compose.yml
```
5. Переходим к /etc/sysctl.conf и редактируем как в прошлом пункте

6. Запускаем докер, для успешного запуска нужно проставить галочку в настройках Docker Desktop
```
sudo service docker.service start
 docker-compose up -d
```
7. Заключительный пункт
```
source /etc/environment
docker stack deploy --compose-file docker-compose.yml opencti
```
8. Теперь мы можем войти в систему с учетными данными, настроенными в 4 пункте.

## Вывод
 В ходе выполнения практической работы, я занчительно улучшил навыки использования терминала в OC Linux, а так же успешно выполнил задачу данной практической работы.
