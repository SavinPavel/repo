# Основы обработки данных с помощью R

## Цель работы
You are a network security administrator for the medium sized business XYZcorp. You often use network flow data to uncover anomalous security events. This challenge provides some sample aggregated data on flows, and uses answers from the anomalous events to construct the flag.

Knowledge of network security or protocols is not required. This challenge requires data stacking, slicing, and/or anomaly detection.

## Исходные данные
1. Ноутбук MSI
2. RStudio
3. timestamp,src,dst,port,bytes
4. Internal hosts have IPs beginning with 12-14
5. External IPs include everything else


## Задание

### Question 1: Discover Data Exfiltration 1

Our intellectual property is leaving the building in large chunks. A machine inside is being used to send out all of our widget designs. One host is sending out much more data from the enterprise than the others. What is its IP?

* Загрузим базу и приведем ее в нормальный вид
* Приведем дату к читаемому формату
* Произведем выборку среди источников, дабы определить принадлежит ли он компании
* Группируем источники компании и суммируем их трафик, выводим самый крупный
`
```{r}
df <- vroom::vroom("../input/2019-trendmicro-ctf-wildcard-400/gowiththeflow_20190826.csv",
                  col_names = F,
                  col_types = c(X1 = 'd', X2 = 'c', X3 = 'c', X4 = 'i', X5 = 'i'))
colnames(df)<-c('timestamp','src','dst','port','bytes')
options(digits.secs=3)
df <- df %>% mutate(rtime = lubridate::as_datetime(timestamp/1000)) %>% select(2:6)
df %>% head(5)
```

```{r}
## # A tibble: 5 × 5
##   src           dst           port bytes rtime                  
##   <chr>         <chr>        <int> <int> <dttm>                 
## 1 13.43.52.51   18.70.112.62    40 57354 2020-01-06 16:00:00.000
## 2 16.79.101.100 12.48.65.39     92 11895 2020-01-06 16:00:00.005
## 3 18.43.118.103 14.51.30.86     27   898 2020-01-06 16:00:00.006
## 4 15.71.108.118 14.50.119.33    57  7496 2020-01-06 16:00:00.010
## 5 14.33.30.103  15.24.31.23    115 20979 2020-01-06 16:00:00.012
```

```{r}
is_internal_host <- function(ip){
    stringr::str_detect(ip,"^1[2-4].") }
df <- df %>% mutate(is_internal = is_internal_host(src))
df %>% head(5)
```

 ```{r}
## # A tibble: 5 × 6
##   src           dst           port bytes rtime                   is_internal
##   <chr>         <chr>        <int> <int> <dttm>                  <lgl>      
## 1 13.43.52.51   18.70.112.62    40 57354 2020-01-06 16:00:00.000 TRUE       
## 2 16.79.101.100 12.48.65.39     92 11895 2020-01-06 16:00:00.005 FALSE      
## 3 18.43.118.103 14.51.30.86     27   898 2020-01-06 16:00:00.006 FALSE      
## 4 15.71.108.118 14.50.119.33    57  7496 2020-01-06 16:00:00.010 FALSE      
## 5 14.33.30.103  15.24.31.23    115 20979 2020-01-06 16:00:00.012 TRUE
```

```{r}
df %>% filter(is_internal)%>% group_by(src) %>%summarize(agress=sum(bytes)) %>% arrange(agress) %>% tail(1)
```

```{r}
## # A tibble: 1 × 2
##   src               agress
##   <chr>              <dbl>
## 1 13.37.84.125 11152202376
```

### Question 2: Discover Data Exfiltration 2

Another attacker has a job scheduled that export the contents of our internal wiki. One host is sending out much more data during off hours from the enterprise than the others, different from the host in the Question 1. What is its IP?
* В решении используется часть информации полученной в прошлом задании
* Вывожу в отдельное поле час совершения операции
* Добавляю поле "флаг", рабочее время с 16 до 0(Время работы получено путем анализа данных) TRUE если операция в нерабочее время (можно было бы сделать как в первом задании функцию, но машина постоянно перезагружалась)
* Группируем источники компании у которы в поле флага "TRUE" и суммируем их трафик, выводим самый крупный
```{r}
df <- vroom::vroom("../input/2019-trendmicro-ctf-wildcard-400/gowiththeflow_20190826.csv",
                  col_names = F,
                  col_types = c(X1 = 'd', X2 = 'c', X3 = 'c', X4 = 'i', X5 = 'i'))
colnames(df)<-c('timestamp','src','dst','port','bytes')
options(digits.secs=3)
df <- df %>% mutate(rtime = lubridate::as_datetime(timestamp/1000)) %>% select(2:6)
df %>% head(5)
```

```{r}
## # A tibble: 5 × 5
##   src           dst           port bytes rtime                  
##   <chr>         <chr>        <int> <int> <dttm>                 
## 1 13.43.52.51   18.70.112.62    40 57354 2020-01-06 16:00:00.000
## 2 16.79.101.100 12.48.65.39     92 11895 2020-01-06 16:00:00.005
## 3 18.43.118.103 14.51.30.86     27   898 2020-01-06 16:00:00.006
## 4 15.71.108.118 14.50.119.33    57  7496 2020-01-06 16:00:00.010
## 5 14.33.30.103  15.24.31.23    115 20979 2020-01-06 16:00:00.012
```

```{r}
is_internal_host <- function(ip){
    stringr::str_detect(ip,"^1[2-4].") }
df <- df %>% mutate(is_internal = is_internal_host(src))
df %>% head(5)
```

```{r}
## # A tibble: 5 × 6
##   src           dst           port bytes rtime                   is_internal
##   <chr>         <chr>        <int> <int> <dttm>                  <lgl>      
## 1 13.43.52.51   18.70.112.62    40 57354 2020-01-06 16:00:00.000 TRUE       
## 2 16.79.101.100 12.48.65.39     92 11895 2020-01-06 16:00:00.005 FALSE      
## 3 18.43.118.103 14.51.30.86     27   898 2020-01-06 16:00:00.006 FALSE      
## 4 15.71.108.118 14.50.119.33    57  7496 2020-01-06 16:00:00.010 FALSE      
## 5 14.33.30.103  15.24.31.23    115 20979 2020-01-06 16:00:00.012 TRUE
```

```{r}
df <- df %>% mutate(t=hour(rtime))
df %>% head(5)
```

```{r}
## # A tibble: 5 × 7
##   src           dst         port bytes rtime                   is_internal     t
##   <chr>         <chr>      <int> <int> <dttm>                  <lgl>       <int>
## 1 13.43.52.51   18.70.112…    40 57354 2020-01-06 16:00:00.000 TRUE           16
## 2 16.79.101.100 12.48.65.…    92 11895 2020-01-06 16:00:00.005 FALSE          16
## 3 18.43.118.103 14.51.30.…    27   898 2020-01-06 16:00:00.006 FALSE          16
## 4 15.71.108.118 14.50.119…    57  7496 2020-01-06 16:00:00.010 FALSE          16
## 5 14.33.30.103  15.24.31.…   115 20979 2020-01-06 16:00:00.012 TRUE           16
 ```
 
```{r}
df %>% filter(is_internal==TRUE)%>% group_by(t) %>%summarize(agress=sum(bytes))%>%head(24)
```
 
```{r}
## # A tibble: 24 × 2
##        t     agress
##    <int>      <dbl>
##  1     0 4614528945
##  2     1 4600719108
##  3     2 4632736988
##  4     3 4634496077
##  5     4 4604557006
##  6     5 4599740949
##  7     6 4628693052
##  8     7 4843672503
##  9     8 4762471181
## 10     9 4602761878
## # … with 14 more rows
 ```
 
```{r}
df <- df %>%filter(t<16 | t>23)%>% mutate(is_work="TRUE")
df %>% head(5)
```

 ```{r}
## # A tibble: 5 × 8
##   src      dst      port bytes rtime                   is_internal     t is_work
##   <chr>    <chr>   <int> <int> <dttm>                  <lgl>       <int> <chr>  
## 1 18.109.… 13.53.…   105 13759 2020-01-07 00:00:00.013 FALSE           0 TRUE   
## 2 15.90.3… 14.51.…    44  9376 2020-01-07 00:00:00.013 FALSE           0 TRUE   
## 3 13.37.1… 17.94.…    27   993 2020-01-07 00:00:00.019 TRUE            0 TRUE   
## 4 15.68.1… 14.46.…    65 16685 2020-01-07 00:00:00.061 FALSE           0 TRUE   
## 5 14.32.1… 17.44.…    37  6377 2020-01-07 00:00:00.076 TRUE            0 TRUE
 ```
 
```{r}
df %>% filter(is_internal==TRUE & is_work==TRUE)%>% group_by(src) %>%summarize(agress=sum(bytes)) %>% arrange(agress) %>% tail(1)
```

 ```{r}
## # A tibble: 1 × 2
##   src             agress
##   <chr>            <int>
## 1 13.37.84.125 770770035
 ```
 


## Вывод
Нам удалось решить два задания))
