# Практическая работа №1

## Исходные данные

    * Ноутбук MSI
    * ПО Docker Desktop
    * образ ubuntu
    
## Цель работы

1. Создать репозиторий в GitLab.
2. Скачать Docker.
3. Создать образ

## Выполение работы

### Создать репозиторий в GitLab.
1. Регестрируемся в Gitlab через аккаунт Github.
2. Создаем публичный репозиторй P1.

### Скачать Docker.
1. Регестрирумся на https://www.docker.com/
2. Скачиваем и устанавливаем Docker Desktop
3. Настраиваем Docker

### Создаем образ
1. Создаем Dockerfile
```
FROM ubuntu:latest
ADD myscript1.sh /tmp/myscript1.sh
RUN /tmp/myscript1.sh
```
2. Используем команды для создания и запуска образа

```
docker build -t <будущее название>:<таг>
docker run -it --name <название> ubuntu:latest /bin/bash
```
3. Пользуемся созданным образом

## Вывод
 В ходе выполнения практической работыпри помощи Docker Desktop мы смогли  создать образ.
